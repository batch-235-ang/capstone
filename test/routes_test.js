const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
	})

})


/*
===============================================
					Capstone
===============================================
*/

describe("currency_test_suite", () => {
	/*
	===============================================
						1
	===============================================
	*/
	it("test_api_post_currency_if_running", (done) => {
		chai.request("http://localhost:5001")
		.get("/forex/currency")
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	/*
	===============================================
						2
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_name_is_missing", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "ringgit",
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	/*
	===============================================
						3
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_name_is_not_string", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "ringgit",
			"name": {"malaysia": "malaysian ringgit"},
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	/*
	===============================================
						4
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_name_is_empty", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "ringgit",
			"name": "",
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	/*
	===============================================
						5
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_ex_is_missing", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "ringgit",
			"name": "Malaysian Ringgit"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	/*
	===============================================
						6
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_ex_is_not_object", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "ringgit",
			"name": "Malaysian Ringgit",
			"ex": "199"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	/*
	===============================================
						7
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_ex_is_empty", () => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "ringgit",
			"name": "Malaysian Ringgit",
			"ex": {}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
		/*
	===============================================
						8
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_alias_is_missing", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"name": "Malaysian Ringgit",
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
		/*
	===============================================
						9
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_alias_is_not_string", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": {"alias": "ringgit"},
			"name": "Malaysian Ringgit",
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
		/*
	===============================================
						10
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_alias_is_empty", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "",
			"name": "Malaysian Ringgit",
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	/*
	===============================================
						11
	===============================================
	*/
	it("test_api_post_currency_returns_400_if_alias_already_exists", (done) => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "usd",
			"name": "Malaysian Ringgit",
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})
	/*
	===============================================
						12
	===============================================
	*/
	it("test_api_post_currency_returns_200_if_input_is_valid", () => {
		chai.request("http://localhost:5001")
		.post("/forex/currency")
		.type("json")
		.send({
			"alias": "ringgit",
			"name": "Malaysian Ringgit",
			"ex": {
				"peso": 12.73,
				"usd": 0.22,
				"won": 309.95,
				"yuan": 1.56 
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 200);
			done();
		})
	})
})