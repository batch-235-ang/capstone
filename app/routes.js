const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {

		if(!req.body.hasOwnProperty("name")){
			return res.status(400).send({error: "Bad request - required field NAME is missing"});
		}
		if(typeof req.body.name !== "string"){
			return res.status(400).send({error: "Bad request - NAME must be a string"});
		}
		if(req.body.name.length < 1){
			return res.status(400).send({error: "Bad request - NAME must not be empty"});
		}

		if(!req.body.hasOwnProperty("ex")){
			return res.status(400).send({error: "Bad request - Exchange Rates must not be empty"});
		}
		if(typeof req.body.ex !== "object"){
			return res.status(400).send({error: "Bad request  - Exchange Rates must be of type Object"});
		}
		if(req.body.ex.length < 1){
			return res.status(400).send({error: "Bad request - Exchange Rates must not be empty"});
		}

		if(!req.body.hasOwnProperty("alias")){
			return res.status(400).send({error: "Bad request - required field ALIAS is missing"});
		}
		if(req.body.alias.length < 1){
			return res.status(400).send({error: "Bad request - ALIAS must not be empty"});
		}
		if(typeof req.body.alias !== "string"){
			return res.status(400).send({error: "Bad request - ALIAS must be a string"});
		}

		let isRegistered = exchangeRates.find(currency => {return currency.alias === req.body.alias});
		if(isRegistered){
			return res.status(400).send({error: "Bad request - ALIAS already exists"});
		}
		
		if(req.body.hasOwnProperty("name" && "alias", "ex") && req.body.ex.length > 0 && req.body.name.length > 0 && req.body.alias.length > 0 && isRegistered)
			return res.status(200).send({success: "Success"});

	})

module.exports = router;
